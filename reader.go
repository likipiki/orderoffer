package main

import (
	"encoding/json"
	"io/ioutil"
)

const (
	filename = "config.json"
)

type Config struct {
	NodeCount int      `json:"node_count"`
	Names     []string `json:"names"`
	Paths     []Path   `json:"paths"`
	Coords    []Coord  `json:"coords"`
}

type Path struct {
	From  string `json:"from"`
	To    string `json:"to"`
	Delay int    `json:"delay"`
}

type Coord struct {
	X    int    `json:"x"`
	Y    int    `json:"y"`
	Name string `json:"name"`
}

func (c *Config) Check() {
	if len(c.Names) == 0 {
		c.Names = make([]string, 0)
	}
	if len(c.Paths) == 0 {
		c.Paths = make([]Path, 0)
	}
	if len(c.Coords) == 0 {
		c.Coords = make([]Coord, 0)
	}
}

func writeToFile(config Config) error {
	bytes, err := json.Marshal(config)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, bytes, 0644)
	if err != nil {
		return err
	}
	return nil
}

func readFile() (Config, error) {
	config := Config{}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return config, err
	}

	err = json.Unmarshal(b, &config)
	if err != nil {
		return config, err
	}

	return config, nil
}
