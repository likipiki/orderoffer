default:
	go build

exe:
	GOOS=windows GOARCH=386 go build -o myapp.exe
	GOOS=windows GOARCH=amd64 go build -o myapp64.exe

run: default
	./myapp