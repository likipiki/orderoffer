package main

import (
	"errors"
	"html/template"
	"io"

	"github.com/labstack/echo/v4"
)

const (
	templateFolder = "templates/"
)

var (
	config Config
)

// Define the template registry struct
type TemplateRegistry struct {
	templates map[string]*template.Template
}

// Implement e.Renderer interface
func (t *TemplateRegistry) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	tmpl, ok := t.templates[name]
	if !ok {
		err := errors.New("Template not found -> " + name)
		return err
	}
	return tmpl.ExecuteTemplate(w, "base.html", data)
}

func main() {
	var err error
	config, err = readFile()
	if err != nil {
		panic(err)
	}
	config.Check()

	e := echo.New()
	e.Debug = true
	e.Static("/static", "static")

	templates := make(map[string]*template.Template)
	templates["index.html"] = template.Must(template.ParseFiles("templates/index.html", "templates/base.html"))
	templates["select.html"] = template.Must(template.ParseFiles("templates/select.html", "templates/base.html"))
	templates["main.html"] = template.Must(template.ParseFiles("templates/main.html", "templates/base.html"))
	e.Renderer = &TemplateRegistry{
		templates: templates,
	}

	// Pages
	e.GET("/", Index)
	e.GET("/main", Main)

	// Handlers
	e.POST("/save", SaveProcessing)
	e.POST("/erase", EraseProcessing)
	e.POST("/start", StartProcessing)
	e.POST("/selectform", SelectProcessing)
	e.POST("/indexform", IndexProcessing)
	e.POST("/add_path", AddPathProcessing)

	e.Logger.Fatal(e.Start(":1323"))
}
