package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
)

var (
	result []string
	fl     bool
)

type MyNode struct {
	name    string
	visited bool
}

// Deicstra algorithm
type Node struct {
	Name    string
	Index   int
	Visited bool
	Weight  int
}

func Index(c echo.Context) error {
	return c.Render(http.StatusOK, "index.html", map[string]interface{}{
		"nodeCount": config.NodeCount,
	})
}

func IndexProcessing(c echo.Context) error {
	name := c.FormValue("number_node")
	nodeCount, err := strconv.Atoi(name)
	if err != nil {
		return err
	}
	config.NodeCount = nodeCount
	writeToFile(config)

	return c.Render(http.StatusOK, "select.html", map[string]interface{}{
		"nodes": makeRange(1, config.NodeCount),
	})
}

func SelectProcessing(c echo.Context) error {
	names := make([]string, 0)
	for i := 0; i < config.NodeCount; i++ {
		name := c.FormValue("name" + strconv.Itoa(i+1))
		names = append(names, name)
	}

	config.Names = names
	config.Paths = []Path{}
	config.Coords = []Coord{}

	writeToFile(config)

	return c.Redirect(http.StatusSeeOther, "/main")
}

func renderMain(c echo.Context) error {
	bytes, err := json.Marshal(config)
	if err != nil {
		panic(err)
	}

	return c.Render(http.StatusOK, "main.html", map[string]interface{}{
		"nodeCount": config.NodeCount,
		"data":      string(bytes),
	})
}

func Main(c echo.Context) error {
	return renderMain(c)
}

func SaveProcessing(c echo.Context) error {
	coords := []Coord{}

	if err := c.Bind(&coords); err != nil {
		return err
	}

	config.Coords = coords
	writeToFile(config)

	resp := struct {
		Status string `json:"status"`
	}{
		Status: "ok",
	}

	return c.JSON(http.StatusOK, &resp)
}

func AddPathProcessing(c echo.Context) error {
	from := c.FormValue("from")
	to := c.FormValue("to")
	delayStr := c.FormValue("delay")
	delay, err := strconv.Atoi(delayStr)
	if err != nil {
		return err
	}

	config.Paths = append(config.Paths, Path{from, to, delay})
	writeToFile(config)

	return c.Redirect(http.StatusSeeOther, "/main")
}

func EraseProcessing(c echo.Context) error {
	config.Paths = []Path{}
	writeToFile(config)

	return c.Redirect(http.StatusSeeOther, "/main")
}

func StartProcessing(c echo.Context) error {
	from := c.FormValue("from")
	to := c.FormValue("to")

	bytes, err := json.Marshal(config)
	if err != nil {
		panic(err)
	}

	// Create matrix
	matrix := make([][]int, config.NodeCount)
	for i := range matrix {
		matrix[i] = make([]int, config.NodeCount)
	}

	// Initialize matrix by default values
	for i := 0; i < config.NodeCount; i++ {
		for j := 0; j < config.NodeCount; j++ {
			matrix[i][j] = -1
		}
	}

	weights, pathLen, maxWeight, toIndex := deikstra(matrix, from, to)

	w := make([]MyNode, config.NodeCount)
	for i := 0; i < config.NodeCount; i++ {
		w[i] = MyNode{
			config.Names[i],
			false,
		}
	}

	gto := 0
	for _, el := range weights {
		if el.Name == to {
			gto = el.Index
			break
		}
	}
	fl = true

	fromNumber := 0
	for i, el := range config.Names {
		if el == from {
			fromNumber = i
			break
		}
	}

	find([]string{}, matrix, w, fromNumber, gto, weights[toIndex].Weight, 0)
	deikstraPath := make([]string, 0)
	for _, el := range result {
		deikstraPath = append(deikstraPath, el)
	}
	fmt.Println("dddd", deikstraPath)

	max := -1
	d1, d2 := 0, 0
	for i, name1 := range config.Names {
		for j, name2 := range config.Names {
			if name1 != name2 {
				_, pathLen, _, _ := deikstra(matrix, name1, name2)
				if pathLen > max {
					max = pathLen
					d1 = i
					d2 = j
				}
			}
		}
	}

	for i := 0; i < config.NodeCount; i++ {
		w[i] = MyNode{
			config.Names[i],
			false,
		}
	}
	fl = true
	find([]string{}, matrix, w, d1, d2, max, 0)

	return c.Render(http.StatusOK, "main.html", map[string]interface{}{
		"nodeCount": config.NodeCount,
		"result": map[string]interface{}{
			"from":        from,
			"to":          to,
			"path":        strings.Join(deikstraPath, " - "),
			"delay":       pathLen,
			"maxDelay":    maxWeight,
			"diameter":    strings.Join(result, " - "),
			"diameterNet": max,
		},
		"data": string(bytes),
	})
}

// return weights, pathLen, maxWeight, toIndex
func deikstra(matrix [][]int, from, to string) ([]Node, int, int, int) {
	fromNumber := 0
	toNumber := 0

	weights := make([]Node, config.NodeCount)

	for i := 0; i < config.NodeCount; i++ {
		weights[i] = Node{
			Name:    config.Names[i],
			Index:   i,
			Visited: false,
			Weight:  9999,
		}
	}

	toIndex := 0
	for i := 0; i < config.NodeCount; i++ {
		if weights[i].Name == from {
			weights[i].Weight = 0
		}
		if weights[i].Name == to {
			toIndex = weights[i].Index
		}
	}

	fmt.Println(weights)

	maxWeight := -1
	for _, path := range config.Paths {

		for _, weight := range weights {
			if weight.Name == path.From {
				fromNumber = weight.Index
			}
			if weight.Name == path.To {
				toNumber = weight.Index
			}
		}
		matrix[fromNumber][toNumber] = path.Delay
		matrix[toNumber][fromNumber] = path.Delay

		if path.Delay > maxWeight {
			maxWeight = path.Delay
		}
	}

	fmt.Println(weights)
	for i := 0; i < config.NodeCount; i++ {
		for j := 0; j < config.NodeCount; j++ {
			fmt.Print(matrix[i][j])
			fmt.Print(" ")
		}
		fmt.Println()
	}

	for {

		// Select minimum node
		minNodeValue := 100000
		minNodeIndex := 0

		for _, node := range weights {
			if (node.Weight < minNodeValue) && (!node.Visited) {
				minNodeValue = node.Weight
				minNodeIndex = node.Index
			}
		}

		weights[minNodeIndex].Visited = true

		for i := 0; i < config.NodeCount; i++ {
			if matrix[minNodeIndex][i] != -1 {
				if weights[i].Weight > minNodeValue+matrix[minNodeIndex][i] {
					weights[i].Weight = minNodeValue + matrix[minNodeIndex][i]
				}
			}
		}

		fl := true
		for _, weight := range weights {
			if !weight.Visited {
				fl = false
				break
			}
		}

		if fl {
			break
		}
	}
	return weights, weights[toIndex].Weight, maxWeight, toIndex
}

func find(path []string, matrix [][]int, ww []MyNode, cur int, to int, end int, d int) {
	ww[cur].visited = true
	path = append(path, ww[cur].name)

	if d > end {
		return
	}

	if cur == to {
		if (d == end) && fl {
			result = path
			fl = false
			return
		}
		return
	}

	for i := 0; i < config.NodeCount; i++ {
		if (matrix[cur][i] != -1) && (!ww[i].visited) {
			find(path, matrix, ww, i, to, end, d+matrix[cur][i])
		}
	}

}
