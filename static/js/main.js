const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const height = 500;
const weidth = 800;

const dotW = 12;

let startX = 10;
let startY = 10;
let step = 50;

var BB = canvas.getBoundingClientRect();
var offsetX = BB.left;
var offsetY = BB.top;
var WIDTH = canvas.width;
var HEIGHT = canvas.height;

// drag related variables
var dragok = false;

// an array of objects that define different nodes
var rects = [];

if (data.coords | data.coords.length > 0) {
    for (const node of data.coords) {
        rects.push({
            x: node.x,
            y: node.y,
            width: 12,
            height: 12,
            fill: 'red',
            isDragging: 'false',
            name: node.name,
        });
    }
} else {
    for (const node of data.names) {
        rects.push({
            x: startX,
            y: startY,
            width: 12,
            height: 12,
            fill: 'red',
            isDragging: 'false',
            name: node,
        });

        startX += step;
        startY += step;
    }
}

// listen for mouse events
canvas.onmousedown = myDown;
canvas.onmouseup = myUp;
canvas.onmousemove = myMove;

// call to draw the scene
draw();

// draw a single rect
function rect(x, y, w, h) {
    ctx.beginPath();
    ctx.rect(x, y, w, h);
    ctx.closePath();
    ctx.fill();
}

// clear the canvas
function clear() {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
}

// redraw the scene
function draw() {
    clear();
    ctx.fillStyle = "white";
    rect(0, 0, WIDTH, HEIGHT);

    for (var i = 0; i < rects.length; i++) {
        var r = rects[i];
        ctx.fillStyle = r.fill;
        rect(r.x, r.y, r.width, r.height);
        ctx.fillStyle = "black";
        ctx.fillText (r.name, r.x+15, r.y+15);
    }

    ctx.lineWidth = 2;
    ctx.strokeStyle = 'gray';
    for (const path of data.paths) {
        const {from, to, delay} = path;
        let x, y, tx, ty = 0;

        for (let rect of rects) {
            if (rect.name == from) {
                x = rect.x + 6;
                y = rect.y + 6;
            }
            if (rect.name == to) {
                tx = rect.x+6;
                ty = rect.y+6;
            }
        }

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(tx, ty);
        ctx.stroke();
    }

}


// handle mousedown events
function myDown(e) {

    e.preventDefault();
    e.stopPropagation();

    var mx = parseInt(e.clientX - offsetX);
    var my = parseInt(e.clientY - offsetY);

    dragok = false;
    for (var i = 0; i < rects.length; i++) {
        var r = rects[i];
        if (mx > r.x && mx < r.x + r.width && my > r.y && my < r.y + r.height) {
            dragok = true;
            r.isDragging = true;
        }
    }
    startX = mx;
    startY = my;
}


// handle mouseup events
function myUp(e) {
    e.preventDefault();
    e.stopPropagation();

    dragok = false;
    for (var i = 0; i < rects.length; i++) {
        rects[i].isDragging = false;
    }
}


// handle mouse moves
function myMove(e) {
    if (dragok) {

        e.preventDefault();
        e.stopPropagation();

        var mx = parseInt(e.clientX - offsetX);
        var my = parseInt(e.clientY - offsetY);

        var dx = mx - startX;
        var dy = my - startY;

        for (var i = 0; i < rects.length; i++) {
            var r = rects[i];
            if (r.isDragging) {
                r.x += dx;
                r.y += dy;
            }
        }

        draw();

        startX = mx;
        startY = my;

    }
}