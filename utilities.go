package main

func makeRange(start, stop int) []int {
	a := make([]int, stop-start+1)
	for i := range a {
		a[i] = start + i
	}
	return a
}
